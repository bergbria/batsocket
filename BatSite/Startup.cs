﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BatSite.Startup))]
namespace BatSite
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();
        }
    }
}

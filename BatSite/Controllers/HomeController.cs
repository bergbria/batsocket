﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using BatCore;
using BatSite.Hubs;
using Microsoft.AspNet.SignalR;

namespace BatSite.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult RunBatFile(string hubConnectionId)
        {
            BatRunner.RunBatInNewThread((sender, arguments) => SendMessageToClient(hubConnectionId, arguments.Data), hubConnectionId,
                (result) => ForwardBatResult(hubConnectionId, result));

            return Content("Bat File Started");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";


            return View();
        }

        public void BroadcastMessageToClients(string message)
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<BatHub>();
            context.Clients.All.AppendOutput(message);
        }

        public static void SendMessageToClient(string connectionId, string message)
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<BatHub>();
            context.Clients.Client(connectionId).AppendBatOutput(message);
        }

        public static void ForwardBatResult(string connectionId, BatResult result)
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<BatHub>();
            context.Clients.Client(connectionId).HandleBatResult(result);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BatCore
{
    public class BatRunner
    {
        private const string BatPath = @"E:\Users\Brian\Code\Personal\BatSocket\BatCore\Resources\BatScript.bat";

        public static void RunBatInNewThread(DataReceivedEventHandler outputEventHandler, string connectionId, Action<BatResult> resultHandler)
        {
            Thread thread = new Thread(() =>
            {
                ProcessStartInfo processInfo = new ProcessStartInfo
                {
                    WindowStyle = ProcessWindowStyle.Hidden,
                    CreateNoWindow = false,
                    RedirectStandardError = true,
                    RedirectStandardOutput = true,
                    FileName = BatPath,
                    UseShellExecute = false,
                };

                using (Process process = new Process { StartInfo = processInfo })
                {
                    process.OutputDataReceived += outputEventHandler;

                    process.Start();
                    process.BeginOutputReadLine();

                    process.WaitForExit();

                    BatResult result = new BatResult
                    {
                        OutputData = "Bat File Completed Succesfully"
                    };

                    resultHandler(result);
                }
            });

            thread.Start();
        }
    }
}

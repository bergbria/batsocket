### What is BatSocket? ###
BatSocket is a prototype project that uses ASP.NET MVC, AJAX, and SignalR to demonstrate:

* a .bat file being invoked by a website in a separate process
* the standard output of that process being streamed to the website in real time
* a custom result object being pushed to the website client after the process completes

### How do I get set up? ###

* Clone the repo and open in Visual Studio 2013
* Edit the path in BatRunner.cs line 14 to point to a legitimate location on your machine (this should be unnecessary after future commits)